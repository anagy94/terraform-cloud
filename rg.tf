resource "azurerm_resource_group" "rg" {
  name     = var.rg-name
  location = var.rg-location



provisioner "local-exec" {
    command = "echo ${azurerm_resource_group.rg.id} >> rg_id.txt"
  }
}